/**
 * Padding output to always have 2 characters.
 * @param {string} hex one or two characters
 * @returns {string} hex with two characters
 */
const pad = (hex) => {
  return hex.length === 1 ? "0" + hex : hex;
};

module.exports = {
  /**
   * Converts the RGB values to a Hex string
   * @param {number} red 0-255
   * @param {number} green 0-255
   * @param {number} blue 0-255
   * @returns {string} hex value
   */
  rgbToHex: (red, green, blue) => {
    const redHex = red.toString(16);
    const greenHex = green.toString(16);
    const blueHex = blue.toString(16);
    const hex = "#" + pad(redHex) + pad(greenHex) + pad(blueHex);
    return hex;
  },
  /**
   * Converts the Hex string to RGB values
   * @param {string} hex #000000 - #ffffff
   * @returns {number[]} RGB values in an array
   */
  hexToRgb: (hex) => {
    const redRgb = parseInt(hex.slice(1, 3), 16);
    const greenRgb = parseInt(hex.slice(3, 5), 16);
    const blueRgb = parseInt(hex.slice(5, 7), 16);
    const rgb = [redRgb, greenRgb, blueRgb];
    return rgb;
  },
};
