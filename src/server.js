const express = require("express");
const converter = require("./converter");
const app = express();
const port = 3000;

// Welcome endpoint
app.get("/", (req, res) => res.send("Welcome!"));

// RGB to Hex endpoint
app.get("/rgb-to-hex", (req, res) => {
  const red = parseInt(req.query.r, 10);
  const green = parseInt(req.query.g, 10);
  const blue = parseInt(req.query.b, 10);
  const hex = converter.rgbToHex(red, green, blue);
  res.send(hex);
});

// Hex to RGB endpoint
app.get("/hex-to-rgb", (req, res) => {
  const hex = "#" + req.query.hex;
  const rgb = converter.hexToRgb(hex);
  // RGB values in parentheses e.g. (124, 12, 238)
  res.send(`(${rgb[0]}, ${rgb[1]}, ${rgb[2]})`);
});

if (process.env.NODE_ENV === "test") {
  module.exports = app;
} else {
  app.listen(port, () => console.log("Server listening..."));
}

console.log("NODE_ENV: " + process.env.NODE_ENV);
