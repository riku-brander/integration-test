// TDD - unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Colour Code Converter", () => {
  describe("RGB to HEX conversions", () => {
    it("Converts the basic colours", () => {
      const redHex = converter.rgbToHex(255, 0, 0); // #ff0000
      const greenHex = converter.rgbToHex(0, 255, 0); // #00ff00
      const blueHex = converter.rgbToHex(0, 0, 255); // #0000ff

      expect(redHex).to.equal("#ff0000"); // Red hex value
      expect(greenHex).to.equal("#00ff00"); // Green hex value
      expect(blueHex).to.equal("#0000ff"); // Blue hex value
    });
    it("Converts more complicated colours", () => {
      const goldHex = converter.rgbToHex(197, 162, 33); // #c5a221
      const cyanHex = converter.rgbToHex(167, 231, 229); // #a7e7e5
      const purpleHex = converter.rgbToHex(105, 14, 120); // #690e78

      expect(goldHex).to.equal("#c5a221"); // Gold hex value
      expect(cyanHex).to.equal("#a7e7e5"); // Cyan hex value
      expect(purpleHex).to.equal("#690e78"); // Purple hex value
    });
  });
  describe("HEX to RGB conversions", () => {
    it("Converts the basic colours", () => {
      const redRgb = converter.hexToRgb("#ff0000"); // 255, 0, 0
      const greenRgb = converter.hexToRgb("#00ff00"); // 0, 255, 0
      const blueRgb = converter.hexToRgb("#0000ff"); // 0, 0, 255

      expect(redRgb).to.deep.equal([255, 0, 0]); // Red RGB value
      expect(greenRgb).to.deep.equal([0, 255, 0]); // Green RGB value
      expect(blueRgb).to.deep.equal([0, 0, 255]); // Blue RGB value
    });
    it("Converts more complicated colours", () => {
      const goldRgb = converter.hexToRgb("#c5a221"); // 197, 162, 33
      const cyanRgb = converter.hexToRgb("#a7e7e5"); // 167, 231, 229
      const purpleRgb = converter.hexToRgb("#690e78"); // 105, 14, 120

      expect(goldRgb).to.deep.equal([197, 162, 33]); // Gold RGB value
      expect(cyanRgb).to.deep.equal([167, 231, 229]); // Cyan RGB value
      expect(purpleRgb).to.deep.equal([105, 14, 120]); // Purple RGB value
    });
  });
});
