const expect = require("chai").expect;
const request = require("request");
const app = require("../src/server");
const port = 3000;

describe("Colour Code Converter API", () => {
  before("Starting server", (done) => {
    server = app.listen(port, () => {
      done();
    });
  });
  describe("RGB to Hex conversion", () => {
    const baseurl = `http://localhost:${port}`;
    const url = baseurl + "/rgb-to-hex?r=255&g=0&b=0";
    it("Returns status 200", (done) => {
      request(url, (error, response, body) => {
        expect(response.statusCode).to.equal(200);
        done();
      });
    });
    it("Returns the colour in hex", (done) => {
      request(url, (error, response, body) => {
        expect(body).to.equal("#ff0000");
        done();
      });
    });
  });
  describe("Hex to RGB conversion", () => {
    const baseurl = `http://localhost:${port}`;
    const url = baseurl + "/hex-to-rgb?hex=ff0000";
    it("Returns status 200", (done) => {
      request(url, (error, response, body) => {
        expect(response.statusCode).to.equal(200);
        done();
      });
    });
    it("Returns the colour in RGB", (done) => {
      request(url, (error, response, body) => {
        expect(body).to.equal("(255, 0, 0)");
        done();
      });
    });
  });
  after("Stopping server", (done) => {
    server.close();
    done();
  });
});
